# 4PDM

## Introduction

4PDM signifie 4ème prof de mathématiques, pour faire face au manque de temps que disposent nos enseignants et donc leur réticence à réaliser des corrections au TD, ce projet a pour but de par nos connaissance collective de réaliser nous même les corrections.

Le choix du repo Git permet à tout le monde de participer à la réalisation et rectification de ces corrections.<br>
🔄 Cela signifie que les corrections seront réalisées au fur et à mesure et que chaque correction est susceptible d'être modifiée. <br>
🧩 De plus, ces corrections ne proposent qu'une seule solution pour résoudre l'énoncé, un problème pouvant se résoudre en principe de plusieurs manières.

## Soutien

🙏 Si ces corrections vous ont aidé, n'hésitez pas à marquer votre soutien en cliquant sur l'étoile en haut à droite.

<img src="./src/star.PNG">

## Correction


- 📅A1
  - 📁S1
  - 📁S2

- 📅A2
  - 📁S3
    - [📄Proba TD3](https://bit.ly/3hbQUC5) Lois usuelles
    - [📄Proba TD4](https://bit.ly/3FKDA12) Variables aléatoires continues
    - [📄Formules Proba](htpps://bit.ly/3UNmhjW)
    - [📄Crypto TD1](https://bit.ly/3UQei5A) Nombre premiers, PGCD, PPCM
    - [📄Crypto TD2](https://bit.ly/3PhwqnQ) Congruence
  - 📁S4
  

## Participation
Ce projet a besoin de vous, si vous trouvez une erreur, ou vous avez une correction d’exercice à proposer, n'hésitez surtout pas à réaliser un `pull request` pour proposer des modifications ou ajouts. 

## Aide
En cas de problème sur un énoncé vous pouvez ouvrir une `Issue`, qui permettra alors au éléve suivant d'accéder eu aussi au explication.



